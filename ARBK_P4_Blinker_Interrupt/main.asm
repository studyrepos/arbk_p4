; #############################################################################
; ARBK_P4_Blinker_Interrupt.asm
;
; ARBK - FH Aachen - WS2017/2018
;
; Created: 21.10.2017
; Author : Fabian Sch�ttler
; #############################################################################
; Notes:
;
; #############################################################################
.INCLUDE <m8adef.inc>    ; includes controller labels
.CSEG                   ; start code segment

; CPU Frequency (Hz)
.equ f_cpu = 1000000

; define some labels for working registers
.def rbutton_mask   = r2
.def rled_mask      = r3
.def save_reg       = r15

.def state          = r16
.def leds           = r17
.def button_state   = r18
.def button_press   = r19
.def button_flag    = r20
.def wr0            = r23
.def wr1            = r24
.def wr2            = r25

.equ led_port       = portd
.equ led_mask       = 0xC0
.equ led_yel        = 0x40
.equ led_red        = 0x80

.equ button_pin     = pind
.equ button_mask    = 0x0C
.equ button_yel     = 0x04
.equ button_red     = 0x08

; Interrupt Vector Table
.org 0x0000
    rjmp    init            ; Reset vector

.org INT0addr
    rjmp    extInt0

.org INT1addr
    rjmp    extInt1

.org OVF0addr
    rjmp    timer0ovf

; reserve progmem for full vectortable
.org INT_VECTORS_SIZE

; #############################################################################
; ISRs
extInt0:
    in      save_reg, SREG                  ; load SREG
    push    save_reg                        ; push SREG to stack
    cli                                     ; disable interrupts
    ldi     button_press, button_red        ; load button_press
    rcall   debounce                        ; call debounce on button_press
    ldi     button_flag, 1                  ; set flag for main
    pop     save_reg                        ; pop SREG from stack
    out     SREG, save_reg                  ; restore SREG
    reti

extInt1:
    in      save_reg, SREG                  ; load SREG
    push    save_reg                        ; push SREG to stack
    cli                                     ; disable interrupts
    ldi     button_press, button_yel        ; load button_press
    rcall   debounce                        ; call debounce on button_press
    ldi     button_flag, 1                  ; set flag for main
    pop     save_reg                        ; pop SREG from stack
    out     SREG, save_reg                  ; restore SREG
    reti

timer0ovf:
    in      save_reg, SREG
    push    save_reg

    in      r26, led_port                      ; read PORTD
    EOR     r26, leds
    out     led_port, r26                      ; write led bits to port

    pop     save_reg
    out     SREG, save_reg
    reti

;###################################
; debounce button
debounce:
    in      button_state, button_pin        ; read PIN
    andi    button_state, button_mask       ; mask PoI
    cp      button_state, button_press      ; check againts previously pressed button
    breq    debounce                        ; wait for button release
    rjmp    button_end
button_end:
    ldi     wr0, button_mask                ; load button_mask
    EOR     button_press, wr0               ; invert masked bits of button_press
    ret

; #############################################################################
; Init

init:
    ; init stack
    ldi     wr0, HIGH(RAMEND)
    out     SPH, wr0
    ldi     wr0, LOW(RAMEND)
    out     SPL, wr0

    ; init PortD
    ; set PD7, PD6, PD5 as output
    ldi     wr0, led_mask
    out     ddrd, wr0
    ; leds off, PullUps on
    ldi     wr0, led_mask | button_mask
    out     led_port, wr0

    ; preload mask register
    ldi     wr0, led_mask
    mov     rled_mask, wr0
    ldi     wr0, button_mask
    mov     rbutton_mask, wr0

    ; init state machine
    ldi     state, 0

    ;init timer
    in      wr0, TCCR0
    ori     wr0, (1 << CS02) | (1 << CS00)     ; prescaler 1024
    out     TCCR0, wr0
    in      wr0, TIMSK
    ori     wr0, (1 << TOIE0)                  ; enable overflow isr
    out     TIMSK, wr0

    ; activate external interrupts
    in      wr0, GICR
    ori     wr0, (1 << INT0) | (1 << INT1)
    out     GICR, wr0

    ; activate global interrupts
    sei

; entry for main loop
loop:
    cpi     button_flag, 1                  ; button_flag set?
    brne    loop                            ; break if not

    ldi     button_flag, 0                  ; reset button_flag
    rjmp    state_yellow                    ; jump to state machine

; state machine
state_yellow:    ; yellow blinking
    cpi     state, 1                        ; check state
    brne    state_red                       ; branch if not state 1
    cpi     button_press, 0                 ; check for pressed buttons
    breq    state_end                       ; branch if no button pushed
trans_yellow_off:                           ; transiton from yellow to off
    cpi     button_press, button_yel        ; is button_yellow pressed?
    brne    trans_yellow_red                ; if not, button_red is pressed
    ldi     leds, 0
    ldi     state, 0                        ; switch to state_off
    rjmp    state_end
trans_yellow_red:                           ; transition from yellow to red
    ldi     leds, led_red                   ; mask bit for red led
    ldi     state, 2                        ; switch to state 2
    rjmp    state_end

state_red:    ; red blinking
    cpi     state, 2                        ; check state
    brne    state_off                       ; branch to state off, if state != 2
    cpi     button_press, 0                 ; check for pressed buttons
    breq    state_end                       ; branch if no button pressed
trans_red_off:                              ; transition from red to off
    cpi     button_press, button_red        ; is button_red pressed
    brne    trans_red_yellow                ; if not, button_yellow is pressed
    ldi     leds, 0                         ; unmask all leds
    ldi     state, 0                        ; switch to state 0
    rjmp    state_end
trans_red_yellow:                           ; transition from red to yellow
    ldi     leds, led_yel                   ; mask bit for yellow led
    ldi     state, 1                        ; switch to state 1
    rjmp    state_end

state_off:    ; off
    ldi     leds, 0                         ; unmask all (failsafe)
    cpi     button_press, 0                 ; check for pressed buttons
    breq    state_end                       ; branch if no button pressed
trans_off_yellow:                           ; transition from off to yellow
    cpi     button_press, button_yel        ; is button_yellow pressed?
    brne    trans_off_red                   ; if not, button_red is pressed
    ldi     leds, led_yel                   ; mask bits for yellow led
    ldi     state, 1                        ; switch to state 1
    rjmp    state_end
trans_off_red:                              ; transition from off to red
    ldi     leds, led_red                   ; mask bit for red led
    ldi     state, 2                        ; switch to state 2
    rjmp    state_end

state_end:  ; output
    in      wr0, PORTD                      ; read PORTD
    andi    wr0, (~led_mask)                ; delete led bits
    or      wr0, leds                       ; set new led bits
    ldi     wr1, led_mask
    EOR     wr0, wr1
    out     PORTD, wr0                      ; write led bits to port
    rjmp    loop

; -----------------------------------------------------------------------------